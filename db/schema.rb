# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190206054126) do

  create_table "counts", force: :cascade do |t|
    t.text "name"
    t.integer "total_call", default: 0
    t.float "av_acd"
    t.float "av_pp"
    t.integer "total_op_time"
    t.date "date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "outbound", default: 0
    t.integer "mail", default: 0
    t.integer "add_data", default: 0
  end

  create_table "users", force: :cascade do |t|
    t.string "u_name"
    t.string "login_id"
    t.string "password_digest"
    t.string "user_image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
