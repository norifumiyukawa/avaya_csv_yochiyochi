class ChangeDefaultToCounts1 < ActiveRecord::Migration[5.1]
  
  def up
    
    change_column_default :counts, :total_call, 0
  end
 
  def down
    change_column_default :counts, :total_call, NULL
  end
  
end
