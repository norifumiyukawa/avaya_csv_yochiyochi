class CreateUsersTable < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
    	t.string :u_name
      t.string :login_id
      t.string :password
      t.string :user_image      

      t.timestamps
    end
  end
end
