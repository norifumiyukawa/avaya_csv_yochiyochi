class AddColumnCounts < ActiveRecord::Migration[5.1]
  def change
  	add_column :counts, :outbound, :integer, default: 0
  	add_column :counts, :mail, :integer, default: 0
  end
end
