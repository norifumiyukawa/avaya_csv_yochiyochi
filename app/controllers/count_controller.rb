class CountController < ApplicationController
before_action :not_login_user, {only: [:import, :del, :upload]}
helper_method :sort_column, :sort_direction
  
  require "date"
  def index
    @max_new = Count.maximum('date')
    if @max_new
    @new_date = Count.where(:date => @max_new).order(sort_column + ' ' + sort_direction)
    @yesterday = @max_new.yesterday
    @tomorrow = @max_new.tomorrow

    @yes = @yesterday.strftime("%Y-%m-%d")
    @tom = @tomorrow.strftime("%Y-%m-%d")
    end
  end
  
  def import
    if params[:file]
  	 Count.import(params[:file].path)
      redirect_to root_path, notice: "CSVのインポートが完了しました"
    elsif !params[:tsv].blank?
      tsv_to_csv = params[:tsv].chomp.split("\t").join(",")
      File.open("public/tsv.csv","w") { |file| file.print(tsv_to_csv) }
      Count.import("public/tsv.csv")
      FileUtils.rm("public/tsv.csv")
      redirect_to root_path, notice: "テキストのインポートが完了しました"
    else
       redirect_to upload_path, alert: "ファイル、またはテキストが空です"
    end
    rescue
      redirect_to upload_path, alert: "ファイル形式、またはテキスト形式が正しくありません"
  end

  def getdate
    @date = Count.where(date: Date.parse(params[:date])).order(sort_column + ' ' + sort_direction)
    @today = Date.parse(params[:date])
    @yesterday = @today.yesterday
    @tomorrow = @today.tomorrow
    
    @yes = @yesterday.strftime("%Y-%m-%d") 
    @tom = @tomorrow.strftime("%Y-%m-%d")
  end
  def del
    destroy_at = Count.where(date: Date.parse(params[:date]))
    destroy_at.destroy_all
    redirect_to root_path, notice: "対象の日付のデータを削除しました" 
  end
  
  def upload
  end


# ソート
private
  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
 
  def sort_column
      Count.column_names.include?(params[:sort]) ? params[:sort] : "total_call"
  end

  def sort_direction_op
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "desc"
  end
 
  def sort_column_op
      Count.column_names.include?(params[:sort]) ? params[:sort] : "date"
  end

end
