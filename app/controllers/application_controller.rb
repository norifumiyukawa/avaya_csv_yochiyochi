class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
# アクション追加
  before_action :set_current_user
  
  def set_current_user
    @current_user = User.find_by(id: session[:user_id])
  end
  
  def not_login_user
    if @current_user == nil
      redirect_to login_path, notice: "ログインしてください"
    end
  end
  
  def already_login
    if @current_user
      redirect_to users_path, notice: "既にログイン済みです"
    end
  end

# end

end
