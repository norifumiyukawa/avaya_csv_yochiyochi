class UsersController < ApplicationController
  # @current_userのIDと、URL（pramsの）IDが異なる場合
  before_action :not_login_user, {only: [:index, :show, :edit, :update, :new, :create]}
  # 既にログイン済みの場合
  before_action :already_login, {only: [:login_form, :login]}
  # before_actionで別ユーザーの編集の制限 edit,update
  before_action :not_this_user, {only: [:edit, :update]}

# 管理者系処理
  # 管理者一覧
  def index
  	@users = User.all
  end
  # 管理者個別ページ
  def show
  	@user = User.find_by(id: params[:id])
  end
  # 新規管理者登録ページ
  def new
  	@user = User.new
  end
  # 管理者登録
  def create
    @user = User.new(user_params)
  	@user.user_image = "default.jpg"
  	if @user.save
  		redirect_to users_path, notice: "ユーザー登録が完了しました"
  	else
  		render("users/new")
  	end
  end
  # before_actionで別ユーザーの編集の制限 edit,update
  def not_this_user
    if @current_user.id != params[:id].to_i
      redirect_to users_path, notice: "自分ではないユーザーのため編集する権限がありません"
    end
  end
  # ユーザー編集 editページとupdate処理
  def edit
    @user = User.find_by(id: params[:id])
  end
  def update
    @user = User.find_by(id: params[:id])
    if @user.update(user_params)
          if user_params[:user_image]
            @user.user_image = "#{@user.id}.jpg"
            image = user_params[:user_image]
            File.binwrite("public/user_images/#{@user.user_image}", image.read)
            @user.save
          end
      redirect_to users_path, notice: "ユーザー情報を編集しました"
    else
      render("users/edit")
    end
  end

# ログイン系の処理
  # フォーム表示
  def login_form
  end
  # ログイン処理
  def login
    @user = User.find_by(login_id: login_params[:login_id])
    if @user && @user.authenticate(login_params[:password])
      session[:user_id] = @user.id
      redirect_to users_path, notice: "ログインしました"
    else
      @error_message = "ログインIDまたはパスワードが間違っています"
      @login_id = params[:login_id]
      @password = params[:password]
      render("users/login_form")
    end
  end
  # ログアウト処理
  def logout
    reset_session
    redirect_to login_path, notice: "ログアウトしました"
  end
  
  #パラメータ系
  private
  def user_params
    params.require(:user).permit(:u_name, :login_id, :password, 
    :password_confirmation, :user_image)
  end
  def login_params
    params.require(:user).permit(:login_id, :password)
  end
end