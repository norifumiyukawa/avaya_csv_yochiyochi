class Count < ApplicationRecord
	 require "date"

  def self.import(file)
    int = 0 
    @place_key = "東京CS"
    CSV.foreach(file) do |row|
      int += 1
      if int == 1
        @place = row[1]
      end
      if @place != @place_key
        redirect_to "/import"
      end
      if int == 2
        @about_date = row[1].to_date
      end
      if int >= 6 && row[1].to_i != 0
      count = Count.new   
      count.name = row[0]
      count.total_call  = row[1].to_i 
      count.av_acd = row[2].to_i
      count.total_op_time = row[1].to_i * row[2].to_i
      count.av_pp = row[3].to_i
      count.date = @about_date.strftime("%Y%m%d")   
      count.save
      end
    end
  end

end