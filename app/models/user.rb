class User < ApplicationRecord
	validates :u_name, {presence: true, uniqueness: true}
	validates :login_id, {presence: true, uniqueness: true}
	has_secure_password
	validates :password, presence: true
end
