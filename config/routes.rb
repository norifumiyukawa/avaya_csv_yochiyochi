Rails.application.routes.draw do
 root "count#index"
 resources :users, :except => [:destroy]
 get 'login', to: 'users#login_form'
 get '/logout' => "users#logout"
 post 'login', to: 'users#login'

 resources 'count', only: :index do
  collection { post :import}
 end
 
 post "/count/call_outbound/:id/update" => "count#update_c_o"
 get "/count/call_outbound/:id" => "count#c_o"
 get "/upload" => "count#upload"
 get "/count/:date" => "count#getdate"
 get "/count/user/:op_name" => "count#op_name"
 get '/ser' => "count#ser"
 post "/del" => "count#del"
 
end
